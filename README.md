# Test task
## Example of account-based app

This project is a short example of a double-entry app


##Implimentation
You should have Composer and PHP8.0 to install project locally
### How to Install
- clone project
```
git clone git@gitlab.com:alkinoy/account-example.git
```
- go to the app directory
```
cd account-example
```
- run composer
```
composer install
```
- run
```
./bin/console t:t
```

I hope here will be all right.

Please not - this is just double-entry approach demonstrator. I didn't add cache, tests, etc.

## What can be improved
- Tests not implemented (done in different project)
- Entity Manager may be added to store data in DB  
- Add more metrics in statistics
