<?php

declare(strict_types=1);

namespace App\Entity;

use App\Service\AccountInterface;
use DateTimeInterface;
use DateTimeImmutable;

class InMemoryAccount extends AbstractEntity implements AccountInterface
{
    private bool $negativeAccepted = false;
    private float $currentBalance = 0.0;
    private array $balanceHistory = [];
    private string $accountName;

    /**
     * InMemoryAccount constructor.
     * @param string $accountName
     */
    public function __construct(int $id, string $accountName)
    {
        $this->id = $id;
        $this->accountName = $accountName;
    }

    public function setAcceptNegative(bool $accept = true): AccountInterface
    {
        $this->negativeAccepted = $accept;

        return $this;
    }

    public function isAccountAcceptNegative(): bool
    {
        return $this->negativeAccepted;
    }

    public function getBalance(DateTimeInterface $onDate = null): float
    {
        if (null === $onDate) {
            return $this->currentBalance;
        }

        //bruteforce search
        $lastFound = 0.0;
        foreach ($this->balanceHistory as $item) {
            if ($item['date']->getTimestamp() <= $onDate->getTimestamp()) {
                $lastFound = $item['balance'];
            }
        }

        return $lastFound;
    }

    public function increase(float $amount): AccountInterface
    {
        $operationDate = new DateTimeImmutable();
        $this->currentBalance += $amount;
        $this->balanceHistory[] =
            ['date' => $operationDate, 'increased' => $amount, 'decreased' => 0, 'balance' => $this->currentBalance];

        return $this;
    }

    public function decrease(float $amount): AccountInterface
    {
        $operationDate = new DateTimeImmutable();
        $this->currentBalance -= $amount;
        $this->balanceHistory[] =
            ['date' => $operationDate, 'increased' => 0, 'decreased' => $amount, 'balance' => $this->currentBalance];

        return $this;
    }

    public function getBalanceHistory(): array
    {
        return $this->balanceHistory;
    }

    public function getAccountName(): string
    {
        return $this->accountName;
    }
}