<?php

declare(strict_types=1);

namespace App\Entity;

use App\Service\AccountInterface;
use App\Service\LogbookInterface;
use DateTimeImmutable;

class InMemoryLogbook extends AbstractEntity implements LogbookInterface
{
    private array $history = [];
    private int $recordId = 1;

    public function addTransaction(string $operationId, AccountInterface $from, AccountInterface $to, float $amount): void
    {
        $actionDate = new DateTimeImmutable();
        $this->history[] = [
            'id' => $this->recordId++,
            'accountId' => $from->getId(),
            'accountName' => $from->getAccountName(),
            'amount' => -1 * $amount,
            'operationId' => $operationId,
            'date' => $actionDate,
        ];

        $this->history[] = [
            'id' => $this->recordId++,
            'accountId' => $to->getId(),
            'accountName' => $to->getAccountName(),
            'amount' => $amount,
            'operationId' => $operationId,
            'date' => $actionDate,
        ];
    }

    public function showHistory(): array
    {
        return $this->history;
    }

    public function checkZero(): bool
    {
        $balance = 0;
        foreach ($this->history as $item) {
            $balance += $item['amount'];
        }

        return 0.0 === round($balance, 2);
    }
}