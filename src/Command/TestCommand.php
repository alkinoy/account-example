<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\InMemoryAccount;
use App\Repository\InMemoryAccountRepository;
use App\Repository\InMemoryLogbookRepository;
use App\Service\PaymentProcessor;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    private PaymentProcessor $processor;
    private InMemoryAccountRepository $accountRepository;
    private InMemoryLogbookRepository $logbookRepository;

    /**
     * TestCommand constructor.
     * @param PaymentProcessor $processor
     * @param InMemoryAccountRepository $accountRepository
     * @param InMemoryLogbookRepository $logbookRepository
     */
    public function __construct(
        PaymentProcessor $processor,
        InMemoryAccountRepository $accountRepository,
        InMemoryLogbookRepository $logbookRepository
    ) {
        parent::__construct();

        $this->processor = $processor;
        $this->accountRepository = $accountRepository;
        $this->logbookRepository = $logbookRepository;
    }


    protected function configure()
    {
        $this->setName('test:test');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ownAccount = new InMemoryAccount(1, 'Our account');
        $bankAccount = new InMemoryAccount(2, 'Bank account');
        $cashbookAccount = new InMemoryAccount(3, 'Cashbook account');

        $logbook = $this->logbookRepository->getLogbook();

        $client1Account = new InMemoryAccount(4, 'Client 1');
        $client2Account = new InMemoryAccount(5, 'Client 2');

        $this->accountRepository->storeAccount($ownAccount);
        $this->accountRepository->storeAccount($bankAccount);
        $this->accountRepository->storeAccount($cashbookAccount);
        $this->accountRepository->storeAccount($client1Account);
        $this->accountRepository->storeAccount($client2Account);

        $this->processor->setBankAccount($bankAccount);
        $this->processor->setCashBook($cashbookAccount);
        $this->processor->setOwnAccount($ownAccount);

        $startDate = new \DateTimeImmutable();
        $output->writeln(["Started at " . $startDate->format('Y.m.d H:i:s'), '']);

        //add 100 to client1
        $output->writeln("Add 100 to Client1 account");
        $this->processor->addCash($client1Account, 100);

        $output->writeln("Check zero: " . ($logbook->checkZero() ? 'OK' : 'Error'));
        $this->formatHistory($output, $logbook->showHistory());
        $output->writeln('Our account state: ' . $ownAccount->getBalance());
        $output->writeln('Bank account state: ' . $bankAccount->getBalance());
        $output->writeln('Client_1 account state: ' . $client1Account->getBalance());
        $output->writeln('Client_2 account state: ' . $client2Account->getBalance());
        $output->writeln(['', '']);

        $output->write("Sleep 2 secs...");
        sleep(2);
        $output->writeln(["Ok", '']);

        $output->writeln("Add more 100 to Client1 account");
        $this->processor->addCash($client1Account, 100);

        $output->writeln("Check zero: " . ($logbook->checkZero() ? 'OK' : 'Error'));
        $this->formatHistory($output, $logbook->showHistory());
        $output->writeln('Our account state: ' . $ownAccount->getBalance());
        $output->writeln('Bank account state: ' . $bankAccount->getBalance());
        $output->writeln('Client_1 account state: ' . $client1Account->getBalance());
        $output->writeln('Client_2 account state: ' . $client2Account->getBalance());
        $output->writeln(['', '']);

        $output->write("Sleep 2 secs...");
        sleep(2);
        $output->writeln(['Ok', '']);

        $output->writeln("Do transfer 50 from Client1 to Client2");
        $this->processor->makeTransfer($client1Account, $client2Account, 50);
        $output->writeln("Check zero: " . ($logbook->checkZero() ? 'OK' : 'Error'));
        $this->formatHistory($output, $logbook->showHistory());
        $output->writeln('Our account state: ' . $ownAccount->getBalance());
        $output->writeln('Bank account state: ' . $bankAccount->getBalance());
        $output->writeln('Client_1 account state: ' . $client1Account->getBalance());
        $output->writeln('Client_2 account state: ' . $client2Account->getBalance());
        $output->writeln(['', '']);


        $output->write("Sleep 2 secs...");
        sleep(2);
        $output->writeln(['Ok', '']);

        $output->writeln("Try to transfer too much (500) from Client1 to Client2");
        try {
            $this->processor->makeTransfer($client1Account, $client2Account, 500);
        } catch (\Exception $e) {
            $output->writeln('Exception occurred: ' . $e->getMessage());
        }

        $output->writeln("Check zero: " . ($logbook->checkZero() ? 'OK' : 'Error'));
        $this->formatHistory($output, $logbook->showHistory());
        $output->writeln('Our account state: ' . $ownAccount->getBalance());
        $output->writeln('Bank account state: ' . $bankAccount->getBalance());
        $output->writeln('Client_1 account state: ' . $client1Account->getBalance());
        $output->writeln('Client_2 account state: ' . $client2Account->getBalance());
        $output->writeln(['', '']);

        $output->write("Sleep 2 secs...");
        sleep(2);
        $output->writeln(['Ok', '']);

        $output->writeln("Try to transfer too much (500) from Client1 to Client2 but negative allowed!");
        try {
            $client1Account->setAcceptNegative(true);
            $this->processor->makeTransfer($client1Account, $client2Account, 500);
        } catch (\Exception $e) {
            $output->writeln('=>>> Exception occurred: ' . $e->getMessage());
        }

        $output->writeln("Check zero: " . ($logbook->checkZero() ? 'OK' : 'Error'));
        $this->formatHistory($output, $logbook->showHistory());
        $output->writeln('Our account state: ' . $ownAccount->getBalance());
        $output->writeln('Bank account state: ' . $bankAccount->getBalance());
        $output->writeln('Client_1 account state: ' . $client1Account->getBalance());
        $output->writeln('Client_2 account state: ' . $client2Account->getBalance());
        $output->writeln(['', '']);


        $output->writeln(["==========================", '']);

        for ($i = 1; $i < 10; $i++) {
            $output->writeln(["Client_1 account state on +$i sec from start: "
                . $client1Account->getBalance($startDate->add(new \DateInterval('PT' . $i . 'S'))), '']);
        }

        return 0;
    }


    private function formatHistory(OutputInterface $output, array $history): void
    {
        $table = new Table($output);
        $table->setHeaders(['id', 'account Id', 'account Name', 'amount', 'operation Id', 'operation Date']);
        $opId = null;

        foreach ($history as $element) {
            if (null === $opId) {
                $opId = $element['operationId'];
            } elseif ($element['operationId'] !== $opId) {
                $table->addRow(new TableSeparator());
                $opId = $element['operationId'];
            }

            $table->addRows([
                [
                    $element['id'],
                    $element['accountId'],
                    $element['accountName'],
                    $element['amount'],
                    $element['operationId'],
                    $element['date']->format('Y.m.d H:i:s'),
                ],
            ]);


        }

        $table->render();
    }
}