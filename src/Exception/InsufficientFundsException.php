<?php

declare(strict_types=1);

namespace App\Exception;

class InsufficientFundsException extends PayerRuntimeException
{
}