<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\InMemoryLogbook;
use App\Service\AccountInterface;
use App\Service\LogbookInterface;
use App\Service\LogbookRepositoryInterface;

class InMemoryLogbookRepository implements LogbookRepositoryInterface
{
    private LogbookInterface $logbook;

    public function __construct()
    {
        //inmemory repo - just create logbook here
        $this->logbook = new InMemoryLogbook();
    }

    public function addTransaction(string $operationId, AccountInterface $from, AccountInterface $to, float $amount): void
    {
        $this->logbook->addTransaction($operationId, $from, $to, $amount);
    }

    public function getLogbook(): ?LogbookInterface
    {
        return $this->logbook;
    }
}