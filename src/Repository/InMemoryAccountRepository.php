<?php

declare(strict_types=1);

namespace App\Repository;

use App\Exception\AccountNotFoundException;
use App\Exception\PayerRuntimeException;
use App\Service\AccountInterface;
use App\Service\AccountRepositoryInterface;
use DateTimeInterface;

class InMemoryAccountRepository implements AccountRepositoryInterface
{
    /** @var array|AccountInterface[]  */
    private array $accounts = [];

    public function increase(AccountInterface $account, float $amount): AccountInterface
    {
        $account = $this->getAccountById($account->getId());
        $account->increase($amount);

        return $account;
    }

    public function decrease(AccountInterface $account, float $amount): AccountInterface
    {
        $account = $this->getAccountById($account->getId());
        $account->decrease($amount);

        return $account;
    }

    public function getBalance(AccountInterface $account, DateTimeInterface $onDate = null): float
    {
        $account = $this->getAccountById($account->getId());

        return $account->getBalance($onDate);
    }

    /**
     * @param int $id
     *
     * @return AccountInterface
     *
     * @throws AccountNotFoundException
     */
    public function getAccountById(int $id): AccountInterface
    {
        $account = $this->accounts[$id] ?? null;
        if (null === $account) {
            throw new AccountNotFoundException("Account with id $id not found");
        }
        return  $account;
    }

    public function storeAccount(AccountInterface $account): void
    {
        if (array_key_exists($account->getId(), $this->accounts)) {
            throw new PayerRuntimeException('Account already exists');
        }

        $this->accounts[$account->getId()] = $account;
    }
}