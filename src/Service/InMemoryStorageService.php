<?php

declare(strict_types=1);

namespace App\Service;

class InMemoryStorageService implements StorageServiceInterface
{
    private AccountRepositoryInterface $accountRepository;
    private LogbookRepositoryInterface $logbookRepository;

    /**
     * InMemoryStorageService constructor.
     * @param AccountRepositoryInterface $accountRepository
     * @param LogbookRepositoryInterface $logbookRepository
     */
    public function __construct(
        AccountRepositoryInterface $accountRepository,
        LogbookRepositoryInterface $logbookRepository
    ) {
        $this->accountRepository = $accountRepository;
        $this->logbookRepository = $logbookRepository;
    }

    public function beginTransaction(): void
    {
        //we have no transaction implemented in InMemory
    }

    public function commitTransaction(): void
    {
        //we have no transaction implemented in InMemory
    }

    public function makeTransfer(string $operationId, AccountInterface $from, AccountInterface $to, float $amount): void
    {
        $this->accountRepository->decrease($from, $amount);
        $this->accountRepository->increase($to, $amount);
        $this->logbookRepository->addTransaction($operationId, $from, $to, $amount);
    }
}