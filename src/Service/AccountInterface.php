<?php

declare(strict_types=1);

namespace App\Service;

use DateTimeInterface;

interface AccountInterface
{
    public function setAcceptNegative(bool $accept = true): self;

    public function isAccountAcceptNegative(): bool;

    /** Returns balance on date $onDate. If $onDate is null method returns current balance */
    public function getBalance(DateTimeInterface $onDate = null): float;

    public function increase(float $amount): self;

    public function decrease(float $amount): self;

    public function getId(): int;

    public function getBalanceHistory(): array;

    public function getAccountName(): string;
}