<?php

declare(strict_types=1);

namespace App\Service;

interface StorageServiceInterface
{
    public function beginTransaction(): void;

    public function commitTransaction(): void;

    public function makeTransfer(string $operationId, AccountInterface $from, AccountInterface $to, float $amount): void;
}