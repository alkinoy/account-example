<?php

declare(strict_types=1);

namespace App\Service;

interface LogbookRepositoryInterface
{
    public function addTransaction(
        string $operationId,
        AccountInterface $from,
        AccountInterface $to,
        float $amount
    ): void;
}