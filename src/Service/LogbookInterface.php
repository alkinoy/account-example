<?php

declare(strict_types=1);

namespace App\Service;

interface LogbookInterface
{
    public function addTransaction(string $operationId, AccountInterface $from, AccountInterface $to, float $amount): void;

    public function showHistory(): array;

    public function checkZero(): bool;
}