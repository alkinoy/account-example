<?php

declare(strict_types=1);

namespace App\Service;

use DateTimeInterface;

interface AccountRepositoryInterface
{
    public function increase(AccountInterface $account, float $amount): AccountInterface;

    public function decrease(AccountInterface $account, float $amount): AccountInterface;
}