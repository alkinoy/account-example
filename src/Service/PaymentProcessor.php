<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\InsufficientFundsException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Uid\Uuid;

class PaymentProcessor implements PaymentProcessorInterface
{
    private AccountInterface $ownAccount;
    private AccountInterface $bankAccount;
    private AccountInterface $cashBook;
    private StorageServiceInterface $storage;

    private LoggerInterface $logger;

    public const PAYMENT_PRECISION = 2;

    public function __construct(StorageServiceInterface $storage, LoggerInterface $logger)
    {
        $this->storage = $storage;
        $this->logger = $logger;
    }

    public function setOwnAccount(AccountInterface $ownAccount): void
    {
        $this->ownAccount = $ownAccount;
    }

    public function setBankAccount(AccountInterface $bankAccount): void
    {
        $this->bankAccount = $bankAccount;
    }

    public function setCashBook(AccountInterface $cashBook): void
    {
        $this->cashBook = $cashBook;
    }

    public function addCash(AccountInterface $to, float $amount): void
    {
        $operationId = $this->getUniqueOperationId();
        $this->logger->info(
            'Add cash to client account requested',
            [
                'operationId' => $operationId,
                'toAccount' => $to->getId(),
                'amount' => $amount,
            ]
        );

        $bankCommission = round($amount * $this->getBankCommission(), self::PAYMENT_PRECISION);
        $ourCommission = round($amount * $this->getOnwCommission(), self::PAYMENT_PRECISION);
        $restAmount = round(($amount - $bankCommission - $ourCommission), self::PAYMENT_PRECISION);



        $this->storage->beginTransaction();
        //add cache
        $this->storage->makeTransfer($operationId, $this->cashBook, $this->ownAccount, $amount);
        $this->logger->info(
            'Feed our account',
            [
                'operationId' => $operationId,
                'amount' => $amount,
            ]
        );

        //feed user's account
        $this->storage->makeTransfer($operationId, $this->ownAccount, $to, $restAmount);
        $this->logger->info(
            'Feed client account',
            [
                'operationId' => $operationId,
                'amount' => $restAmount,
            ]
        );

        //feed bank
        $this->storage->makeTransfer($operationId, $this->ownAccount, $this->bankAccount, $bankCommission);
        $this->logger->info(
            'Feed bank account',
            [
                'operationId' => $operationId,
                'amount' => $bankCommission,
            ]
        );

        $this->logger->info(
            'Our commission',
            [
                'operationId' => $operationId,
                'amount' => $ourCommission,
            ]
        );

        $this->storage->commitTransaction();
    }

    /**
     * @param AccountInterface $from
     * @param AccountInterface $to
     * @param float $amount
     *
     * @throws InsufficientFundsException
     */
    public function makeTransfer(AccountInterface $from, AccountInterface $to, float $amount): void
    {
        $operationId = $this->getUniqueOperationId();
        $this->logger->info(
            'Transfer requested',
            [
                'operationId' => $operationId,
                'fromAccount' => $from->getId(),
                'toAccount' => $to->getId(),
                'amount' => $amount,
            ]
        );
        $this->checkSourceAccountPolicy($operationId, $from, $amount);


        $this->storage->makeTransfer($operationId, $from, $to, $amount);
    }

    private function getUniqueOperationId(): string
    {
        return Uuid::v4()->toRfc4122();
    }

    /**
     * @param AccountInterface $account
     * @param float $amount
     *
     * @throws InsufficientFundsException
     */
    private function checkSourceAccountPolicy(string $operationId, AccountInterface $account, float $amount): void
    {
        //here we can insert different policies
        if (
            !$account->isAccountAcceptNegative()
            && $account->getBalance() < $amount
        ) {
            $this->logger->info(
                'Transfer declined: Insufficient funds',
                [
                    'operationId' => $operationId,
                    'fromAccount' => $account->getId(),
                    'amount' => $amount,
                    'currentBalance' => $account->getBalance(),
                    'negativeAllowed' => $account->isAccountAcceptNegative(),
                ]
            );

            throw new InsufficientFundsException('Insufficient funds in the account');
        }
    }

    private function getOnwCommission(): float
    {
        //different policies can be added here
        return 0.005;
    }

    private function getBankCommission(): float
    {
        //different policies can be added here
        return 0.005;
    }
}