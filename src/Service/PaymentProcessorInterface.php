<?php

declare(strict_types=1);

namespace App\Service;

use App\Exception\InsufficientFundsException;

interface PaymentProcessorInterface
{
    public function addCash(AccountInterface $to, float $amount): void;

    /**
     * @param AccountInterface $from
     * @param AccountInterface $to
     * @param float $amount
     *
     * @throws InsufficientFundsException
     */
    public function makeTransfer(AccountInterface $from, AccountInterface $to, float $amount): void;
}